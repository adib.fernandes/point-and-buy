import 'react-native-gesture-handler';
import React from 'react';
import HomeScreen from './src/Screens/HomeScreen'
import AuthenticationScreen from './src/Screens/AuthenticationScreen'
import ConfirmationScreen from './src/Screens/ConfirmationScreen'
import ReceiptScreen from './src/Screens/ReceiptScreen'
import RegisterScreen from './src/Screens/RegisterScreen'
import CameraScreen from './src/Screens/CameraScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="RegisterScreen" headerMode="none">
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="AuthenticationScreen" component={AuthenticationScreen} />
        <Stack.Screen name="CameraScreen" component={CameraScreen} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="ReceiptScreen" component={ReceiptScreen} />
        <Stack.Screen name="ConfirmationScreen" component={ConfirmationScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
