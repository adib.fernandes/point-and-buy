import "react-native-gesture-handler";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { MaterialIcons } from "@expo/vector-icons";

import UserPhoto from "../../Assets/images/profilePhoto.png";
import {
    UserHeaderContent,
    UserNameProfile,
    UserNameProfileBold,
    UserPhotoProfile,
    Wrap,
} from "./style";

export default function HomeScreen() {
    const navigation = useNavigation();
    const insets = useSafeAreaInsets();

    return (
        <Wrap
            style={{
                paddingTop: insets.top,
                paddingBottom: insets.bottom,
            }}
        >
            <MaterialCommunityIcons name="menu" size={30} color="white" />
            <UserHeaderContent>
                <UserPhotoProfile source={UserPhoto} />
                <UserNameProfile>
                    Olá,{` `}
                    <UserNameProfileBold>Marcos</UserNameProfileBold>
                </UserNameProfile>
            </UserHeaderContent>
            <MaterialIcons name="notifications" size={30} color="white" />
        </Wrap>
    );
}
