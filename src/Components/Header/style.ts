import styled from "styled-components/native";

export const Wrap = styled.View`
    background-color: #174b6d;
    height: 110px;
    width: 100%;
    margin-top: 0;
    justify-content: space-between;
    padding: 10px;
    align-items: center;
    flex-direction: row;
`;
export const UserHeaderContent = styled.View`
    justify-content: space-between;
    flex-direction: row;
    align-items: center;
`;
export const UserPhotoProfile = styled.Image`
    border-radius: 25px;
    width: 50px;
    height: 50px;
    margin-right: 5px;
`;

export const UserNameProfile = styled.Text`
    margin-left: 5px;
    font-size: 13px;
    color: #f9f9f9;
`;

export const UserNameProfileBold = styled.Text`
    font-weight: bold;
`;
