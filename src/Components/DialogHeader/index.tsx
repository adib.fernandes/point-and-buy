import React from 'react'
import { DialogHeaderStyled } from './style'

export default function DialogHeader({children}) {
    return(
        <DialogHeaderStyled>
            {children}
        </DialogHeaderStyled>
    )
}