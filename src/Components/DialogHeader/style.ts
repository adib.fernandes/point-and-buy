import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native'

export const DialogHeaderStyled = styled.View`
    background-color: #3498db;
    height: 75px;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    flex-direction: row;
    margin-top: 0px;
    margin-bottom: auto;
    padding: 0 20px;
    padding-top: 25px;
`;

export const DialogText = styled.Text`
    font-size: 14px;
    color: #E5E5E5;
    margin: auto;
`

export const ReturnButton = styled(RectButton)`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    background-color: #3498db;
    margin: auto;
    color: #ffffff;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    position: absolute;
    top: 30px;
    left: 20px;
`