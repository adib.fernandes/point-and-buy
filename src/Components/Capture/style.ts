import styled from "styled-components/native";
import { RectButton } from "react-native-gesture-handler";

export const OpenCamera = styled(RectButton)`
    background-color: #3498db;
    height: 50px;
    width: 100%;
    justify-content: center;
    align-items: center;
    margin-top: auto;
    margin-bottom: 0;
`;