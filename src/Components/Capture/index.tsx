import React from "react";
import { useNavigation } from "@react-navigation/native";
import { OpenCamera } from "./style";
import { Ionicons } from "@expo/vector-icons";

export default function Capture() {

    const navigation = useNavigation();

    return (
        <OpenCamera
            onPress={() => {
                navigation.navigate("CameraScreen")
            }}
        >
            <Ionicons name="ios-qr-scanner" size={30} color="white" />
        </OpenCamera>
    );
}
