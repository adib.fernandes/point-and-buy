import React from 'react'
import { RectButtonProperties } from 'react-native-gesture-handler'
import { DialogFooterStyled } from './style'

const DialogFooter: React.FC<RectButtonProperties> = ({children, ...rest}) => {
    return(
        <DialogFooterStyled {...rest}>
            {children}
        </DialogFooterStyled>
    )
}

export default DialogFooter