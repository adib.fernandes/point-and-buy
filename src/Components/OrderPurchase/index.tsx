import React from "react";
import { Entypo } from "@expo/vector-icons";
import { VerticalLineStart, VerticalLineEnd, Wrap } from "./style";

export interface OrderPurchaseInterface {
    start?: boolean;
    end?: boolean;
}

const OrderPurchase: React.FC<OrderPurchaseInterface> = ({ start, end }) => {
    return (
        <Wrap>
            {start && <VerticalLineStart />}
            <Entypo
                style={[
                    !start && { marginTop: "auto" },
                    !end && { marginBottom: "auto" },
                    { marginVertical: 0, padding: 0 },
                ]}
                name="dot-single"
                size={25}
                color="black"
            />
            {end && <VerticalLineEnd />}
        </Wrap>
    );
};

export default OrderPurchase;
