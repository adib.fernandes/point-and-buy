import styled from "styled-components/native";

export const Wrap = styled.View`
    height: 70px;
    justify-content: space-between;
    flex-direction: column;
`;
export const VerticalLineStart = styled.View`
    height: 23px;
    background-color: #000000;
    width: 2px;
    margin: 0 auto;
`;

export const VerticalLineEnd = styled.View`
    height: 23px;
    background-color: #000000;
    width: 2px;
    margin: 0 auto;
`;
