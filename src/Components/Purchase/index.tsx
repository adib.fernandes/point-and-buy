import React, { useContext } from "react";
import {
    Container,
    InvoiceContent,
    PurchaseCompany,
    PurchaseContent,
    PurchaseDate,
    PurchaseInfoContent,
    PurchasePrice,
} from "./style";
import { monthNames } from "../../Constants/Date";
import { AntDesign } from "@expo/vector-icons";
import OrderPurchase from "../OrderPurchase";
import { PurchasesContext } from "../../Screens/HomeScreen";

export interface PurchaseInterface {
    dateBuy: Date;
    price: Number;
    company: string;
    order: Number;
}

const Purchase: React.FC<PurchaseInterface> = ({
    company,
    dateBuy,
    price,
    order,
}) => {
    const Purchases = useContext(PurchasesContext);
    return (
        <Container>
            <PurchaseDate>{`${dateBuy.getDate()} ${
                monthNames[dateBuy.getMonth()]
            }`}</PurchaseDate>
            <PurchaseContent>
                <OrderPurchase
                    start={order > 0 && true}
                    end={order < Purchases.length - 1 && true}
                />
                <PurchaseInfoContent>
                    <PurchaseCompany>{company}</PurchaseCompany>
                    <PurchasePrice>
                        {`R$ ${price.toString().replace(".", ",")}`}
                    </PurchasePrice>
                </PurchaseInfoContent>
            </PurchaseContent>
            <InvoiceContent>
                <AntDesign name="filetext1" size={20} color="#507193" />
            </InvoiceContent>
        </Container>
    );
};

export default Purchase;
