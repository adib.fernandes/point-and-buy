import styled from "styled-components/native";

export const Container = styled.View`
    justify-content: space-between;
    align-items: center;
    height: 70px;
    background-color: #dddddd;
    width: 100%;
    flex-direction: row;
`;

export const PurchaseDate = styled.Text`
    font-size: 13px;
    color: #507193;
    width: 15%;
    text-align: center;
`;

export const PurchaseContent = styled.View`
    width: 70%;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    height: 100%;
`;

export const PurchaseInfoContent = styled.View`
    width: 70%;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: flex-start;
    height: 100%;
`;

export const PurchaseCompany = styled.Text`
    font-size: 15px;
    color: #000000;
    font-weight: bold;
`;
export const PurchasePrice = styled.Text`
    font-size: 13px;
    color: #000000;
`;

export const InvoiceContent = styled.View`
    width: 15%;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 100%;
    flex: 1;
`;
