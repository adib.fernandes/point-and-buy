import styled from 'styled-components/native'

export const Container = styled.SafeAreaView`
    flex: 1;
`

export const Content = styled.View`
    flex: 1;
    flex-direction: column;
    padding: 20px; 
    background-color: #f9f9f9;
    justify-content: flex-start;
`

export const InputGroup = styled.View`
    width: 100%;
    flex-direction: column;
    justify-content: flex-start;
    margin: 30px 0;
`

export const InputLabel = styled.Text`
    font-size: 15px;
    font-weight: bold;
    color: #3498DB;
    margin-top: 0;
    margin-bottom: 5px;
`

export const RegisterInput = styled.TextInput`
    width: 100%;
    border-bottom-color: #BDC3C7;
    border-bottom-width: 1px;
    margin-top: 10px;
    margin-bottom: auto;
`