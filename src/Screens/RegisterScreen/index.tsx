import { AntDesign } from "@expo/vector-icons";
import React, { useState } from "react";
import DialogHeader from "../../Components/DialogHeader";
import { DialogText, ReturnButton } from "../../Components/DialogHeader/style";
import { Container, InputGroup, InputLabel, RegisterInput } from "./style";
import {useNavigation} from '@react-navigation/native'
import DialogFooter from "../../Components/DialogFooter";
import { Content } from "../RegisterScreen/style";

export default function RegisterScreen() {
    
    const [textInputName, setTextInputName] = useState("")
    const [textInputCPF, setTextInputCPF] = useState("")
    const [textInputTel, setTextInputTel] = useState("")

    const navigation = useNavigation()

    return (
        <Container>
            <DialogHeader>
                <ReturnButton onPress={() => navigation.goBack()}>
                    <AntDesign
                        style={{ margin: "auto" }}
                        name="arrowleft"
                        size={20}
                        color="white"
                    />
                </ReturnButton>
                <DialogText>Registro</DialogText>
            </DialogHeader>
            <Content>
                <InputGroup>
                    <InputLabel>Nome completo</InputLabel>
                    <RegisterInput keyboardType="default" maxLength={100} onChangeText={e => setTextInputName(e)} />
                </InputGroup>
                <InputGroup>
                    <InputLabel>CPF</InputLabel>
                    <RegisterInput keyboardType="numeric" maxLength={14} onChangeText={e => setTextInputCPF(e)} />
                </InputGroup>
                <InputGroup>
                    <InputLabel>Número de celular</InputLabel>
                    <RegisterInput keyboardType="numeric" maxLength={16} onChangeText={e => setTextInputTel(e)} />
                </InputGroup>
            </Content>
            <DialogFooter onPress={() => navigation.navigate("HomeScreen")}>
                <DialogText>Salvar</DialogText>
            </DialogFooter>
        </Container>
    );
}
