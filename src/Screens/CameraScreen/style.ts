import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native'

export const Container = styled.SafeAreaView`
    flex: 1;
    background-color: #f9f9f9;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding-top: 25px;
`

export const AreaCapture = styled.View`
    flex: 1;
    position: absolute;
    top: 0;
    background-color: #f9f9f9;
`