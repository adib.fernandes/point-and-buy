import { BarCodeScanner } from "expo-barcode-scanner";
import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { Container } from "./style";
import { AntDesign } from '@expo/vector-icons'; 
import {useNavigation} from '@react-navigation/native'
import DialogHeader from "../../Components/DialogHeader";
import {DialogText, ReturnButton} from "../../Components/DialogHeader/style"

export default function CameraScreen() {

    const navigation = useNavigation()
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        navigation.navigate("ConfirmationScreen")
    };

    return (
        <Container>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            >
            <DialogHeader>
                <ReturnButton
                    onPress={() => navigation.navigate("HomeScreen")}
                >
                    <AntDesign style={{margin: "auto"}} name="arrowleft" size={20} color="white" />
                </ReturnButton>
                <DialogText>
                    Aponte para o QRCode
                </DialogText>
            </DialogHeader>
            </BarCodeScanner>
        </Container>
    );
}
