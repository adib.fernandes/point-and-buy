import "react-native-gesture-handler";
import React, { createContext } from "react";
import { useNavigation } from "@react-navigation/native";
import Header from "../../Components/Header";
import Capture from "../../Components/Capture";
import { Container, Content, PurchasesHistoric, TitleContent } from "./style";
import Purchase from "../../Components/Purchase";

const Purchases = [
    {
        date: new Date("2020-10-01"),
        price: 13.49,
        company: "Lojas Americanas",
    },
    {
        date: new Date("2020-08-30"),
        price: 151.85,
        company: "Magazine Luiza",
    },
    {
        date: new Date("2020-07-22"),
        price: 99.99,
        company: "Extra",
    },
    {
        date: new Date("2020-04-13"),
        price: 1189.99,
        company: "Amazon",
    },
];

export const PurchasesContext = createContext(Purchases);

export default function HomeScreen() {
    const navigation = useNavigation();

    return (
        <Container>
            <Header />
            <Content>
                <TitleContent>Histórico de compras</TitleContent>
                <PurchasesHistoric>
                    <PurchasesContext.Provider value={Purchases}>
                        {Purchases.map(({ date, price, company }, key) => {
                            return (
                                <Purchase
                                    key={key}
                                    order={key}
                                    dateBuy={date}
                                    price={price}
                                    company={company}
                                />
                            );
                        })}
                    </PurchasesContext.Provider>
                </PurchasesHistoric>
            </Content>
            <Capture />
        </Container>
    );
}
