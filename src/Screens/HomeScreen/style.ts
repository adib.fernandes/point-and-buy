import styled from "styled-components/native";

export const Container = styled.View`
    flex: 1;
    background-color: #e5e5e5;
`;

export const Content = styled.ScrollView`
    flex: 1;
    padding: 20px 10px;
`;

export const TitleContent = styled.Text`
    color: #2c3e50;
    font-size: 20px;
    width: 100%;
    text-align: center;
    margin-bottom: 20px;
`;

export const PurchasesHistoric = styled.ScrollView``;
