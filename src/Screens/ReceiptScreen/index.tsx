import { AntDesign } from "@expo/vector-icons";
import React from "react";
import DialogHeader from "../../Components/DialogHeader";
import { DialogText, ReturnButton } from "../../Components/DialogHeader/style";
import { Container, Content, LargeReceiptText, MediumReceiptText, SmallReceiptText, SubContent } from "./style";
import {useNavigation} from '@react-navigation/native'
import DialogFooter from "../../Components/DialogFooter";

export default function ReceiptScreen() {
    
    const navigation = useNavigation()

    return (
        <Container>
            <DialogHeader>
                <ReturnButton onPress={() => navigation.goBack()}>
                    <AntDesign
                        style={{ margin: "auto" }}
                        name="arrowleft"
                        size={20}
                        color="white"
                    />
                </ReturnButton>
                <DialogText>Comprovante</DialogText>
            </DialogHeader>
            <Content>
                <SubContent style={{marginTop: "auto", marginBottom: 20}}>
                    <LargeReceiptText style={{}}>Resumo da compra</LargeReceiptText>
                </SubContent>
                <SubContent style={{marginTop: 0, marginBottom: 0}}>
                    <MediumReceiptText style={{}}>Estabelecimento</MediumReceiptText>
                    <SmallReceiptText style={{}}>Lojas Americanas</SmallReceiptText>
                </SubContent>
                <SubContent style={{marginTop: 20, marginBottom: "auto"}}>
                    <MediumReceiptText style={{}}>Visa terminada em 9999</MediumReceiptText>
                    <SmallReceiptText style={{}}>Pagamento (2x R$ 50,00)</SmallReceiptText>
                    <SmallReceiptText style={{}}>Pagamento #99999999 aprovado no dia 13 de novembro</SmallReceiptText>
                </SubContent>
            </Content>
            <DialogFooter>
                <DialogText onPress={() => {}}>Enviar para o meu e-mail</DialogText>
            </DialogFooter>
        </Container>
    );
}
