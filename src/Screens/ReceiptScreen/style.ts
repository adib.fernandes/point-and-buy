import styled from 'styled-components/native'

export const Container = styled.SafeAreaView`
    flex: 1;
`

export const LargeReceiptText = styled.Text`
    font-size: 18px;
    text-align: center;
    margin: 2.5px 0;
    color: #000000;
    max-width: 200px;
    font-weight: bold;
`
export const MediumReceiptText = styled.Text`
    font-size: 16px;
    text-align: center;
    margin: 2.5px 0;
    color: #000000;
    max-width: 200px;
    font-weight: bold;
`

export const SmallReceiptText = styled.Text`
    font-size: 14px;
    text-align: center;
    margin: 2.5px 0;
    color: #000000;
    max-width: 200px;
`

export const Content = styled.View`
    flex: 1;
    flex-direction: column;
    padding: 20px; 
    background-color: #f9f9f9;
`

export const SubContent = styled.View`
    flex-direction: column;
    width: 100%;
    align-items: center;
    justify-content: center;
`

