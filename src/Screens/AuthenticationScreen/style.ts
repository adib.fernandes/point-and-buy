import { RectButton } from "react-native-gesture-handler";
import styled from "styled-components/native";

export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: space-evenly;
    align-items: center;
    background-color: #e5e5e5;
`;

export const MethodsSignIn = styled.View`
    flex-direction: column;
    margin-top: auto;
`;

export const Logo = styled.Image`
    width: 150px;
    height: 150px;
    margin-top: auto;
`;

export const CenteredText = styled.Text`
    color: #2c3e50;
    font-size: 20px;
`;
export const Copyrights = styled.Text`
    margin-bottom: 20px;
    margin-top: auto;
    font-size: 14px;
    color: #bdc3c7;
`;

export const Brand = styled.Text`
    color: #2c3e50;
`;

export const BTNMethodSignIn = styled(RectButton)`
    background-color: #ffffff;
    width: 170px;
    height: 50px;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
    margin: 10px 0;
`;

export const BTNLogo = styled.Image`
    width: 32px;
    height: 32px;
`;
