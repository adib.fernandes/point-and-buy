import "react-native-gesture-handler";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
    Container,
    CenteredText,
    Logo,
    MethodsSignIn,
    Copyrights,
    Brand,
    BTNMethodSignIn,
    BTNLogo,
} from "./style";
import LogoImage from "../../Assets/images/LogoImage.png";
import LogoGoogle from "../../Assets/images/LogoGoogle.png";
import LogoFacebook from "../../Assets/images/LogoFacebook.png";

export default function AuthenticationScreen() {
    const navigation = useNavigation();

    return (
        <Container>
            <Logo source={LogoImage} />
            <MethodsSignIn>
                <CenteredText>Crie ou Entre com:</CenteredText>
                <BTNMethodSignIn
                    onPress={() => navigation.navigate("HomeScreen")}
                    style={{
                        elevation: 5,
                        shadowColor: "#000",
                        shadowOpacity: 0.25,
                        shadowOffset: { width: 0, height: 0 },
                        shadowRadius: 5,
                    }}
                >
                    <BTNLogo source={LogoGoogle} />
                </BTNMethodSignIn>
                <BTNMethodSignIn
                    onPress={() => navigation.navigate("HomeScreen")}
                    style={{
                        elevation: 5,
                        shadowColor: "#000",
                        shadowOpacity: 0.25,
                        shadowOffset: { width: 0, height: 0 },
                        shadowRadius: 5,
                    }}
                >
                    <BTNLogo source={LogoFacebook} />
                </BTNMethodSignIn>
            </MethodsSignIn>
            <Copyrights>
                Desenvolvido por <Brand>Tech Boys</Brand>
            </Copyrights>
        </Container>
    );
}
