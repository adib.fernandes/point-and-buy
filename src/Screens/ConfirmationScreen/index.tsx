import React, { useEffect, useState } from "react";
import DialogHeader from "../../Components/DialogHeader";
import { ConfirmeInput, ConfirmeText, Container } from "./style";
import { DialogText, ReturnButton } from "../../Components/DialogHeader/style";
import { AntDesign } from "@expo/vector-icons";
import { useNavigation } from '@react-navigation/native'

export default function ConfirmationScreen() {
    const navigation = useNavigation()
    const [textInput, setTextInput] = useState("")
    
    function handleVerifySMSCode(code: string) {
        navigation.navigate("ReceiptScreen")
    }

    useEffect(() => {
        (textInput.length === 6) && handleVerifySMSCode(textInput)
    }, [textInput])
    return (
        <Container>
            <DialogHeader>
                <ReturnButton onPress={() => navigation.goBack()}>
                    <AntDesign
                        style={{ margin: "auto" }}
                        name="arrowleft"
                        size={20}
                        color="white"
                    />
                </ReturnButton>
                <DialogText>Autenticação via SMS</DialogText>
            </DialogHeader>
            <ConfirmeText>Digite o código que recebeu via SMS</ConfirmeText>
            <ConfirmeInput keyboardType="numeric" maxLength={6} onChangeText={e => setTextInput(e)} />
        </Container>
    );
}
