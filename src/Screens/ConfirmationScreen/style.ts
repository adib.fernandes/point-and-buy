import styled from 'styled-components/native'

export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
    background-color: #f9f9f9;
`

export const ConfirmeText = styled.Text`
    text-align: center;
    font-size: 18px;
    color: #000000;
    margin-top: 0;
    margin-bottom: 10px;
`

export const ConfirmeInput = styled.TextInput`
    width: 200px;
    border-bottom-color: #BDC3C7;
    border-bottom-width: 1px;
    margin-top: 10px;
    margin-bottom: auto;
    text-align: center;
`